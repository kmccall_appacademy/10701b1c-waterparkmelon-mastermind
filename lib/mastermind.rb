class Code
  attr_reader :pegs

  PEGS = {
    "R" => :red,
    "G" => :green,
    "B" => :blue,
    "Y" => :yellow,
    "O" => :orange,
    "P" => :purple
  }

  def initialize(pegs)
    @pegs = pegs
  end

  def self.parse(str)
    colors = str.chars.map(&:upcase)

    if colors.all? {|peg| PEGS.has_key?(peg)}
      Code.new(colors)
    else
      raise "invalid pegs"
    end
  end

  def [](index)
    @pegs[index]
  end

  def self.random
    random_code = []
    4.times do
      random_code << PEGS.keys.sample
    end
    Code.new(random_code)
  end

  def exact_matches(code)
    matches = 0
    (0..3).each do |i|
      matches += 1 if self[i] == code[i]
    end
    matches
  end

  def near_matches(code)
    matches = 0
    self.color_counts.each do |k, v|
      matches += [v, code.color_counts[k]].min if code.color_counts.has_key?(k)
    end
    matches - self.exact_matches(code)
  end

  def color_counts
    counts = Hash.new(0)
    @pegs.each do |color|
      counts[color] += 1
    end
    counts
  end

  def ==(code)
    return false unless code.is_a? Code
    self.pegs == code.pegs
  end
end

class Game
  attr_reader :secret_code

  def initialize(secret_code = Code.random)
    @secret_code = secret_code
  end

  def get_guess
    puts "What is your guess?"
    begin
      Code.parse(gets.chomp)
    rescue
      puts "Parse error. Try again."
    end
  end

  def display_matches(guess)
    puts "You have #{@secret_code.exact_matches(guess)} exact matches"
    puts "You have #{@secret_code.near_matches(guess)} near matches"
  end

  def play
    turns = 0

    until turns > 10
      guess = get_guess
      if guess == @secret_code
        puts "You win!"
        return
      else
        display_matches(guess)
      end
    end

    "You suck!"
  end
end

if __FILE__ == $PROGRAM_NAME
  Game.new.play
end
